// 1.Які існують типи даних у Javascript?
// string;boolean;number;null;undefined;bigInt;object;symbol.

//2.У чому різниця між == і ===? 
//  === Cтрогое сравнение включает сравнение на тип данных и значение, не переводит к одному типу;
// == Не строгое сравнение сравнивает только значение, а тип данных переводит к одному типу;

//3.Що таке оператор?
//Оператор осуществляет какое-то действие, например присваивает или умножает , определяет true/false и тд.

// 4.Технічні вимоги:
// Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
// Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням:
// Are you sure you want to continue? і кнопками Ok, Cancel. 
// Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача. 
// Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
// Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
// Після введення даних додати перевірку їхньої коректності.
// Якщо користувач не ввів ім'я, або при введенні віку вказав не число - запитати ім'я та вік наново
// (при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).

let name = prompt("What is your name?");

while (!name || !isNaN(name)) {
  alert("Enter your name");
  name = prompt("What is your name?");
}

let age = +prompt("How old are you?");

while (isNaN(age)) {
  alert("Enter correct age");
  age = +prompt("How old are you?");
}
if (age < 18) {
  alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
  let ask = confirm("Are you sure you want to continue?");
  if (ask) {
    alert(`Welcome "${name}"`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else {
  alert(`Welcome "${name}"`);
}
